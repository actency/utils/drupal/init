var fs = require( 'fs' );
var path = fs.absolute( fs.workingDirectory + '/node_modules/phantomcss/phantomcss.js' );
var phantomcss = require( path );

var url = "127.0.0.1:80";
if(casper.cli.has('url')) {
  url = casper.cli.get('url');
}

casper.echo(url);
casper.test.begin( 'PhantomCSS tests', function ( test ) {

  phantomcss.init( {
    rebase: casper.cli.get( "rebase" ),
    casper: casper,
    libraryRoot: fs.absolute( fs.workingDirectory + '/node_modules/phantomcss' ),
    screenshotRoot: fs.absolute( fs.workingDirectory + '/screenshots' ),
    failedComparisonsRoot: fs.absolute( fs.workingDirectory + '/failures' ),
    addLabelToFailedImage: false,
    mismatchTolerance: 1,
    failOnCaptureError: true,
    stepTimeout: 5000,
    waitTimeout: 10000,
    timeout: 15000,
  } );

  casper.options.pageSettings.resourceTimeout = 20000;

  casper.on( 'remote.message', function ( msg ) {
    this.echo( msg );
  } );

  casper.on( 'error', function ( err ) {
    this.die( "PhantomJS has errored: " + err );
  } );

  casper.on( 'resource.error', function ( err ) {
    casper.log( 'Resource load error: ' + err, 'warning' );
  } );

  /*
    The test scenario
  */

  casper.start( url );

  casper.viewport( 1900, 1600 );

  casper.then( function () {

    // wait for modal to fade-in
    casper.waitForSelector( '#wrapper',
        function success() {
          phantomcss.screenshot({
            top: 0,
            left: 0,
            width: 1900,
            height: 1600
          }, 'home')
        },
        function timeout() {
          casper.test.fail( 'Should see home page' );
        }
    );
  } );

  casper.then( function now_check_the_screenshots() {
    // compare screenshots
    phantomcss.compareAll();
  } );

  /*
  Casper runs tests
  */
  casper.run( function () {
    console.log( '\nTHE END.' );
    phantomcss.getExitStatus() // pass or fail?
    casper.test.done();
  } );
} );