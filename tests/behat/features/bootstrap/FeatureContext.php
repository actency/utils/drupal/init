<?php

use Behat\Gherkin\Node\PyStringNode;
use Symfony\Component\BrowserKit\Cookie;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Mink\Exception\ExpectationException;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Custom features context.
 */
class FeatureContext extends RawDrupalContext {

  /**
   * Set up test environment.
   *
   * @param \Behat\Behat\Hook\Scope\BeforeScenarioScope $scope
   *   The scope.
   *
   * @BeforeScenario
   */
  public function setUpTestEnvironment(BeforeScenarioScope $scope) {
    $this->currentScenario = $scope->getScenario();
  }

  /**
   * Generate the report.
   *
   * @param \Behat\Behat\Hook\Scope\AfterStepScope $scope
   *   The scope.
   *
   * @AfterStep
   */
  public function afterStep(AfterStepScope $scope) {
    // If test has failed, and is not an api test, get screenshot.
    if (!$scope->getTestResult()->isPassed()) {
      // Create filename string.
      $featureFolder = preg_replace('/\W/', '', $scope->getFeature()->getTitle());

      $scenarioName = 'unknown';
      foreach ($scope->getFeature()->getScenarios() as $scenario) {
        foreach ($scenario->getSteps() as $step) {
          if ($step->getLine() == $scope->getStep()->getLine()) {
            $scenarioName = $scenario->getTitle();
            break 2;
          }
        }
      }
      $fileName = preg_replace('/\W/', '', $scenarioName) . '.png';

      // Create screenshots directory if it doesn't exist.
      if (!file_exists('../../tests/behat/report/html/assets/screenshots/' . $featureFolder)) {
        mkdir('../../tests/behat/report/html/assets/screenshots/' . $featureFolder, 0755, TRUE);
      }

      // For Selenium2 Driver you can use:
      file_put_contents('../../tests/behat/report/html/assets/screenshots/' . $featureFolder . '/' . $fileName, $this->getSession()->getDriver()->getScreenshot());
    }
  }

  /**
   * Accept cookie sentence.
   *
   * @Given I accept all cookies compliance
   */
  public function iSetCookieCompliance() {
    $this->getSession()->setCookie("cookie-agreed", 2);
    $categorie = urlencode('["required","statistics","preferences","targeting"]');
    $this->getSession()->setCookie("cookie-agreed-categories", $categorie);
    $this->getSession()->reload();
  }

  /**
   * Switches to the main window.
   *
   * @Given /^I switch to the main windows$/
   */
  public function switchToWindow() {
    $this->getSession()->switchToWindow();
  }

  /**
   * Switches focus to an iframe.
   *
   * @param string $iframe_name
   *   The iframe name.
   *
   * @Given /^I switch (?:away from|to) the iframe "([^"]*)"$/
   */
  public function iSwitchToTheIframe($iframe_name) {
    if ($iframe_name) {
      $this->getSession()->switchToIFrame($iframe_name);
    }
    else {
      $this->getSession()->switchToIFrame();
    }
  }

  /**
   * Sentence to check a radio button.
   *
   * @When /^I check the "([^"]*)" radio button$/
   */
  public function iCheckTheRadioButton($radioLabel) {
    $radioButton = $this->getSession()->getPage()->findField($radioLabel);
    if (NULL === $radioButton) {
      throw new Exception("Cannot find radio button " . $radioLabel);
    }
    $radioButton->getAttribute("value");
    $this->getSession()->getDriver()->click($radioButton->getXPath());
  }

  /**
   * Sentence to display an url.
   *
   * @Then /^I want to see the URL$/
   *
   * @throws \Exception
   */
  public function iWantToSeeTheUrl() {
    try {
      $url = $this->getSession()->getCurrentUrl();
      var_dump($url);
    }
    catch (Exception $e) {
      throw new Exception($e);
    }
  }

  /**
   * Sentence to scroll to the top.
   *
   * @Then /^I scroll to the top$/
   *
   * @throws \Exception
   */
  public function iScrollToTheTop() {
    $this->getSession()->executeScript('window.scrollTo(0,0);');
  }

  /**
   * Sentence to print the html page.
   *
   * @Then /^I want to see the page content$/
   *
   * @throws \Exception
   */
  public function iWantToSeeThePageContent() {
    try {
      $html = $this->getSession()->getPage()->getHtml();
      print($html);
    }
    catch (Exception $e) {
      throw new Exception($e);
    }
  }

  /**
   * Sentence to wait for som seconds.
   *
   * @param int $seconds
   *   The time to wait in seconds.
   *
   * @Given /^I wait (\d+) seconds$/
   */
  public function iWaitSeconds(int $seconds) {
    sleep($seconds);
  }

  /**
   * Sentence to set a cookie.
   *
   * @param string $name
   *   The cookie name.
   * @param string $value
   *   The cookie value.
   *
   * @Given I set cookie :name :value
   */
  public function iSetCookie(string $name, string $value) {
    $this->getSession()->setCookie($name, $value);
  }

  /**
   * Sentence to enable xdebug.
   *
   * @Then I enable xdebug
   */
  public function iEnableXdebug() {
    $cookie = new Cookie('XDEBUG_SESSION', 'phpstorm');
    $this->getSession()->setRequestHeader('Cookie', (string) $cookie);
  }

  /**
   * Sentence to click the back button of the broswer.
   *
   * @Then I click the back button of the navigator
   */
  public function iClickTheBackButtonInNavigator() {
    $this->getSession()->getDriver()->back();
  }

  /**
   * Sentence to click on an element.
   *
   * @param string $selector
   *   The css selector.
   *
   * @Given I click the :arg1 element
   */
  public function iClickTheElement(string $selector) {
    $page = $this->getSession()->getPage();
    $element = $page->find('css', $selector);

    if (empty($element)) {
      throw new Exception("No html element found for the selector ('$selector')");
    }

    $element->click();
  }

  /**
   * Custom click action for complex elements.
   *
   * @param string $selector
   *   The css selector of the element.
   *
   * @Then I click the :arg1 overlapped element
   */
  public function iClickThOverlappedElement(string $selector) {
    $page = $this->getSession()->getPage();
    $element = $page->find('css', $selector);

    if (empty($element)) {
      throw new Exception("No html element found for the selector ('$selector')");
    }

    $xpath = $element->getXpath();

    $this->getSession()
      ->executeScript('document.evaluate("' . $xpath . '", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();');
  }

  /**
   * Sentence to select the first element of type.
   *
   * @param string $selector
   *   The css selector.
   *
   * @Given I select the first element in :arg1 list
   */
  public function iSelectTheFirstElement(string $selector) {
    $page = $this->getSession()->getPage();

    $options = $page->findAll('css', "#$selector option");

    /** @var \Behat\Mink\Element\NodeElement $option */
    foreach ($options as $option) {
      if (strcmp($option->getValue(), "_none") != 0) {
        $page->selectFieldOption($selector, $option->getValue());
        return;
      }
    }

    throw new Exception("Unable to find a non empty value.");
  }

  /**
   * Click some text.
   *
   * @param string $text
   *   The text to click on.
   *
   * @When /^I click on the text "([^"]*)"$/
   */
  public function iClickOnTheText(string $text) {
    $session = $this->getSession();
    $element = $session->getPage()->find(
      'xpath',
      $session->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="' . $text . '"]')
    );
    if (NULL === $element) {
      throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
    }

    $element->click();
  }

  /**
   * Click some text in element.
   *
   * @When /^I click on the text "([^"]*)" in "([^"]*)" element$/
   */
  public function iClickOnTheTextInElement($text, $selector) {
    $page = $this->getSession()->getPage();
    $elements = $page->findAll('css', $selector);
    $error = TRUE;
    if (!empty($elements) && is_array($elements)) {
      /** @var \Behat\Mink\Element\NodeElement $element */
      foreach ($elements as $element) {
        if (!empty($element)) {
          $value = $element->getText();
          if (!empty($value)) {
            if (strcmp($value, $text) === 0) {
              $error = FALSE;
              $element->click();
              break;
            }
          }
        }
      }
      if ($error) {
        throw new Exception("Element $text not found in $selector.");
      }
    }
    else {
      throw new Exception("Selector $selector not found.");
    }
  }

  /**
   * Sentence to detect if a select list contains an option.
   *
   * @param string $element
   *   The css id of the element.
   * @param \Behat\Gherkin\Node\PyStringNode $list
   *   The list.
   *
   * @Then /^the selectbox "([^"]*)" should have a list containing:$/
   */
  public function shouldHaveListContaining($element, PyStringNode $list) {
    $page = $this->getSession()->getPage();
    $validStrings = $list->getStrings();

    $elements = $page->findAll('css', "#$element option");

    $option_none = 0;

    /** @var \Behat\Mink\Element\NodeElement $element */
    foreach ($elements as $element) {
      $value = $element->getValue();
      if (strcmp($value, '_none') == 0) {
        $option_none = 1;
        continue;
      }

      if (!in_array($element->getValue(), $validStrings)) {
        throw new Exception("Element $value not found.");
      }
    }

    if ((count($elements) - $option_none) < count($validStrings)) {
      throw new Exception("Expected options are missing in the select list.");
    }
    elseif ((count($elements) - $option_none) > count($validStrings)) {
      throw new Exception("There are more options than expected in the select list.");
    }
  }

  /**
   * Wait for AJAX to finish.
   *
   * @param int $seconds
   *   The time to wait in seconds.
   *
   * @Given I wait max :arg1 seconds for AJAX to finish
   */
  public function iWaitForAjaxToFinish(int $seconds) {
    $condition = <<<JS
    (function() {
      function isAjaxing(instance) {
        return instance && instance.ajaxing === true;
      }
      var d7_not_ajaxing = true;
      if (typeof Drupal !== 'undefined' && typeof Drupal.ajax !== 'undefined' && typeof Drupal.ajax.instances === 'undefined') {
        for(var i in Drupal.ajax) { if (isAjaxing(Drupal.ajax[i])) { d7_not_ajaxing = false; } }
      }
      var d8_not_ajaxing = (typeof Drupal === 'undefined' || typeof Drupal.ajax === 'undefined' || typeof Drupal.ajax.instances === 'undefined' || !Drupal.ajax.instances.some(isAjaxing))
      return (
        // Assert no AJAX request is running (via jQuery or Drupal) and no
        // animation is running.
        (typeof jQuery === 'undefined' || (jQuery.active === 0 && jQuery(':animated').length === 0)) &&
        d7_not_ajaxing && d8_not_ajaxing
      );
    }());
JS;
    $result = $this->getSession()->wait($seconds * 1000, $condition);
    if (!$result) {
      throw new \RuntimeException('Unable to complete AJAX request.');
    }
  }

  /**
   * Sentence to submit a form.
   *
   * @param string $id_form
   *   The form css id.
   *
   * @Then I submit the form with id :arg1
   */
  public function iSubmitTheFormWithId(string $id_form) {
    $node = $this->getSession()->getPage()->findById($id_form);
    if ($node) {
      $this->getSession()->executeScript('jQuery("#' . $id_form . '").submit();');
    }
    else {
      throw new \RuntimeException('form with id ' . $id_form . ' not found');
    }
  }

  /**
   * Sentence to reset the session.
   *
   * @Given I reset the session
   */
  public function iResetTheSession() {
    $this->getSession()->reset();
  }

  /**
   * Sentence to detect if a select option is selected.
   *
   * @param string $optionValue
   *   The option value.
   * @param string $select
   *   The css selector of the select list.
   *
   * @Then /^the option "([^"]*)" from select "([^"]*)" is selected$/
   */
  public function theOptionFromSelectIsSelected(string $optionValue, string $select) {
    $selectField = $this->getSession()->getPage()->find('css', $select);
    if (NULL === $selectField) {
      throw new \Exception(sprintf('The select "%s" was not found in the page %s', $select, $this->getSession()
        ->getCurrentUrl()));
    }

    $optionField = $selectField->find('xpath', "//option[@selected='selected']");
    if (NULL === $optionField) {
      throw new \Exception(sprintf('No option is selected in the %s select in the page %s', $select, $this->getSession()
        ->getCurrentUrl()));
    }

    if ($optionField->getValue() != $optionValue) {
      throw new \Exception(sprintf('The option "%s" was not selected in the page %s, %s was selected', $optionValue, $this->getSession()
        ->getCurrentUrl(), $optionField->getValue()));
    }
  }

  /**
   * Sentence to scroll to an element.
   *
   * @param string $elementId
   *   The css id of the element.
   *
   * @Then I scroll :selector into view
   */
  public function scrollIntoView($elementId) {
    $function = <<<JS
      (function(){
        var elem = document.getElementById("$elementId");
        elem.scrollIntoView({block: "start"});
      })()
      JS;
    try {
      $this->getSession()->executeScript($function);
    }
    catch (Exception $e) {
      throw new \Exception("ScrollIntoView failed");
    }
  }

  /**
   * Fills in specified field with date.
   *
   * Examples:
   *   When I fill in "field_ID" with date "now"
   *   When I fill in "field_ID" with date "-7 days"
   *   When I fill in "field_ID" with date "+7 days"
   *   When I fill in "field_ID" with date "-/+0 weeks"
   *   When I fill in "field_ID" with date "-/+0 years".
   *
   * @param string $field
   *   The field name or identifier.
   * @param string $value
   *   The date.
   *
   * @When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with date "(?P<value>(?:[^"]|\\")*)"$/
   */
  public function fillDateField(string $field, string $value) {
    $newDate = strtotime("$value");

    $dateToSet = date("d/m/Y", $newDate);
    $this->getSession()->getPage()->fillField($field, $dateToSet);
  }

  /**
   * Fills in specified field with date.
   *
   * Examples:
   *   When I fill in "field_ID" with date "now" in the format "m/d/Y".
   *   When I fill in "field_ID" with date "-7 days" in the format "m/d/Y".
   *   When I fill in "field_ID" with date "+7 days" in the format "m/d/Y".
   *   When I fill in "field_ID" with date "-/+0 weeks" in the format "m/d/Y".
   *   When I fill in "field_ID" with date "-/+0 years" in the format "m/d/Y".
   *
   * @param string $field
   *   The field name or identifier.
   * @param string $value
   *   The date.
   * @param string $format
   *   The date format.
   *
   * @When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with date "(?P<value>(?:[^"]|\\")*)" in the format "(?P<format>(?:[^"]|\\")*)"$/
   */
  public function fillDateFieldFormat(string $field, string $value, string $format) {
    $newDate = strtotime("$value");

    $dateToSet = date($format, $newDate);
    $this->getSession()->getPage()->fillField($field, $dateToSet);
  }

  /**
   * Sentence to fill a wysiwyg field.
   *
   * @param string $locator
   *   The field locator.
   * @param string $value
   *   The value to set into the wysiwyg field.
   *
   * @Then I fill in wysiwyg on field :locator with :value
   */
  public function iFillInWysiwygOnFieldWith(string $locator, string $value) {
    $el = $this->getSession()->getPage()->findField($locator);

    if (empty($el)) {
      throw new ExpectationException('Could not find WYSIWYG with locator: ' . $locator, $this->getSession());
    }

    $fieldId = $el->getAttribute('id');

    if (empty($fieldId)) {
      throw new Exception('Could not find an id for field with locator: ' . $locator);
    }

    $this->getSession()
      ->executeScript("
        const domEditableElement = document.querySelector(\"#" . $fieldId . "+div .ck-editor__editable\");
        if (domEditableElement.ckeditorInstance) {
          const editorInstance = domEditableElement.ckeditorInstance;
          if (editorInstance) {
            editorInstance.setData(\"$value\");
          }
        }
      ");
  }

  /**
   * Sentence to detect the breadcrumb presence.
   *
   * @param string $arg1
   *   The link href.
   *
   * @Then I should see the breadcrumb link :arg1
   */
  public function iShouldSeeTheBreadcrumbLink(string $arg1) {
    /**
     * @var Behat\Mink\Element\NodeElement $breadcrumb
     */
    $breadcrumb = $this->getSession()->getPage()->find('css', 'div.block-system-breadcrumb-block');

    // This does not work for URLs.
    $link = $breadcrumb->findLink($arg1);
    if ($link) {
      return;
    }

    // Filter by url.
    $link = $breadcrumb->findAll('css', "a[href=\"{$arg1}\"]");
    if ($link) {
      return;
    }

    // Filter by text.
    $page = $this->getSession()->getPage();

    foreach ($page->findAll('css', 'li.breadcrumb-item') as $element) {
      if ($element->getText() == $arg1) {
        return;
      }
    }

    throw new \Exception(
      sprintf("Expected link %s not found in breadcrumb on page %s",
        $arg1,
        $this->getSession()->getCurrentUrl())
    );
  }

}
