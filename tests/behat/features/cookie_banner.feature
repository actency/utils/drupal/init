Feature: Cookie Banner
  @api @cit @javascript @standard
  In order to test the cookie banner

  Scenario: The cookie banner is present and acceptable
    When I am on the homepage
    Then I should see an ".cookiesjsr-banner--action" element
    And I should see an ".cookiesjsr-btn.important" element
    Given I click the ".cookiesjsr-btn.important" element
    Then I should not see an ".cookiesjsr-btn.important" element
