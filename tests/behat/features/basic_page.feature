Feature: Check basic page CT
  @api @cit @standard @javascript
  In order to create a page
  As an admin
  I want to access /node/add/page
  So that I can create a page

  Scenario: Basic Page CT
    Given I am logged in as a user with the "administrator" role
    When I go to "/node/add/page"
    And I enter "Basic page title" for "title[0][value]"
    And I press "edit-field-paragraph-add-more-browse"
    Given I wait for AJAX to finish
    And I press "field_paragraph_wysiwyg_add_more"
    Given I wait for AJAX to finish
    Then I fill in wysiwyg on field "field_paragraph[0][subform][field_wysiwyg][0][value]" with "Test"
    When I press "edit-submit--2--gin-edit-form"
    Then I should see the success message containing "Basic Page"
