Feature: Default
  @api @cit @standard
  In order to test out the basics

  Scenario: Homepage is reachable and contains all elements
    Given I am not logged in
    When I am on the homepage
    # After install the homepage is not yet created so we should have a 404.
    Then the response status code should be 404
